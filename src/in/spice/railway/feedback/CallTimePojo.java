package in.spice.railway.feedback;

import java.util.Date;

public class CallTimePojo {

	private String callTime = "";
	private String obdCurrentStatus = "";
	private Date callingDate;
	private Date curDate;
	
	public String getCallTime() {
		return callTime;
	}
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	public String getObdCurrentStatus() {
		return obdCurrentStatus;
	}
	public void setObdCurrentStatus(String obdCurrentStatus) {
		this.obdCurrentStatus = obdCurrentStatus;
	}
	
	public Date getCallingDate() {
		return callingDate;
	}
	public void setCallingDate(Date callingDate) {
		this.callingDate = callingDate;
	}
	
	public Date getCurDate() {
		return curDate;
	}
	public void setCurDate(Date curDate) {
		this.curDate = curDate;
	}
	@Override
	public String toString() {
		return "CallTimePojo [callTime=" + callTime + ", obdCurrentStatus=" + obdCurrentStatus + ", callingDate="
				+ callingDate + ", curDate=" + curDate + "]";
	}
	
	
}
