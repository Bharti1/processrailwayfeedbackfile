package in.spice.railway.feedback;

public class TrainOBD {
	
	private String trainNumber;
	private String trainClass;
	
	public String getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getTrainClass() {
		return trainClass;
	}
	public void setTrainClass(String trainClass) {
		this.trainClass = trainClass;
	}
	
	
	
	@Override
	public String toString() {
		return "TrainOBD [trainNumber=" + trainNumber + ", trainClass=" + trainClass + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((trainClass == null) ? 0 : trainClass.hashCode());
		result = prime * result
				+ ((trainNumber == null) ? 0 : trainNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrainOBD other = (TrainOBD) obj;
		if (trainClass == null) {
			if (other.trainClass != null)
				return false;
		} else if (!trainClass.equals(other.trainClass))
			return false;
		if (trainNumber == null) {
			if (other.trainNumber != null)
				return false;
		} else if (!trainNumber.equals(other.trainNumber))
			return false;
		return true;
	}

}
