package in.spice.railway.feedback;

import in.spice.railway.feedback.db.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;

public class Station45List {
	
//	public static List<String> trainList = new ArrayList<String>();
	
	public static Set<String> station45List = new HashSet<String>();

	public static Set<String> getStation45List() {
		return station45List;
	}



	public static void setStation45List(Set<String> station45List) {
		Station45List.station45List = station45List;
	}



	public static Set<String> insertIntoStationList() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("select station_code from tbl_ecatering_stn");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String stationCode = rs.getString(1);
				station45List.add(stationCode);
//				String lang = rs.getString("lang");
//				obdLangMap.put(trainNumber, lang);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}
		return station45List;
	}

}
