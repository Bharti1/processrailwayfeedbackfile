package in.spice.railway.feedback;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import in.spice.railway.feedback.db.DatabaseManager;

public class TrainOBDMap {
	

	public static Map<TrainOBD, String> insertIntoMap(String obd) {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<TrainOBD, String> trainOBDMap = new HashMap<TrainOBD, String>();
		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("select * from obd_info_testing where obd = '"+obd+"'");
			rs = stmt.executeQuery();
			while (rs.next()) {
			//	String obds = rs.getString("obd");
				TrainOBD trainOBD = new TrainOBD();
				trainOBD.setTrainClass(rs.getString("class"));
				trainOBD.setTrainNumber(rs.getString("train_number"));
				trainOBDMap.put(trainOBD, obd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}
		return trainOBDMap;
	}

}
