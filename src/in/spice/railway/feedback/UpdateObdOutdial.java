package in.spice.railway.feedback;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.dbutils.DbUtils;

import in.spice.railway.feedback.db.DatabaseManager;

public class UpdateObdOutdial {

	public static void getTotalStnCount(String obd,String stationCode, String trainNumber) {
		Connection connection = null,con1 = null;
		PreparedStatement stmt = null,pstmt = null;
		ResultSet rs = null;
		int count=0;
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		String callingDate=dateFormat.format(date);
		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("SELECT COUNT(STATUS) AS COUNT FROM tbl_outdial_testing WHERE station_code='"+stationCode+"' and train_no='"+trainNumber+"' AND calling_time like '"+callingDate+"%' AND obd="+obd+" AND STATUS='I' ");
			rs = stmt.executeQuery();
			if (rs.next()) {
				 count=rs.getInt("COUNT");
			}
			
			if(count > 0){
				con1=DatabaseManager.getConnection();
				pstmt=con1.prepareStatement("update tbl_outdial_testing set obd=0 where station_code='"+stationCode+"' and train_no='"+trainNumber+"' AND calling_time like '"+callingDate+"%'  AND obd="+obd+" and STATUS='I'");
				pstmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(con1);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(pstmt);
			DbUtils.closeQuietly(rs);
		}
		
	}
}
