package in.spice.railway.feedback;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import in.spice.railway.feedback.db.DatabaseManager;

public class EcateringTrains {

	public static List<String> listOfTrains() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		List<String> trainNumbers = new ArrayList<String>();

		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("SELECT train FROM tbl_ecatering_trains");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String trainNumber = rs.getString(1);
				trainNumbers.add(trainNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}

		return trainNumbers;
	}
}
