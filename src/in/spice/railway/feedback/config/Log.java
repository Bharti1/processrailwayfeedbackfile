package in.spice.railway.feedback.config;

import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

public class Log{
	
	//private static String path = "E:/logs/";
	
	private static String path = "C:\\Users\\ch-e01455\\Documents\\cdrs";
	
	 private static synchronized void writeLog(String strLogFilePath,String strSubDir, String logString) {

	        String strFileName = "";

	        String strDateDir = "";

	        Calendar objCalendarRightNow = Calendar.getInstance();

	        int intMonth = objCalendarRightNow.get(Calendar.MONTH) + 1;

	        int intDate = objCalendarRightNow.get(Calendar.DATE);

	        int intHour = objCalendarRightNow.get(Calendar.HOUR_OF_DAY);

	        int intMinute = objCalendarRightNow.get(Calendar.MINUTE);

	        int intSecond = objCalendarRightNow.get(Calendar.SECOND);

	        int intMilliSecond = objCalendarRightNow.get(Calendar.MILLISECOND);

	        String strYear = "" + objCalendarRightNow.get(Calendar.YEAR);

	        strDateDir = strLogFilePath+ "/" + intDate + "-" + intMonth + "-" + strYear;
	        createDateDir(strDateDir);
	        strFileName =strDateDir+"/"+strSubDir+"-"+intDate+"-"+intMonth+"-"+strYear + ".log";

	        try {

	            FileWriter out = new FileWriter(strFileName, true);

	            String strLogString =  intDate + "-" + intMonth + "-" + strYear+ " " + intHour + ":" + intMinute + ":" + intSecond + ":" + intMilliSecond + " @"+ logString + "\n";

	            out.write(strLogString);

	            out.close();

	        } catch (Exception ex) {

	            //System.out.println("//System.out.println:" + ex.toString());

	            ex.printStackTrace();

	            System.exit(0);

	        }

	    }



	private synchronized static void createDateDir(String dateDir) {
		try {
			String folderName = dateDir;
			new File(folderName).mkdirs();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getProcessLogs(String logString) {
		writeLog(path,"ProcessLogs", logString);
	}
	
	public static void getErrorLogs(String fileName, String logString) {
		writeLog(path,fileName, logString);
	}
	
	public static void getTimeLogs(String logString) {
		writeLog(path,"TimeLogs", logString);
	}

}