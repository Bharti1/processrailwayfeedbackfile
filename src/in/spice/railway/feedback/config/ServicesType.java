package in.spice.railway.feedback.config;

public class ServicesType {
	
	public static final String service_0="service_0";
	public static final String service_1="service_1";
	public static final String service_2="service_2";
	
	public static final String train_0="0";
	public static final String train_1="1";
	public static final String train_2="2";
	
	public static final String station_0="0";
	public static final String station_1="1";
	public static final String station_2="2";
	
	public static final long req_bed_roll_count=40;
	public static final long req_train_clean_count=60;
	public static final long req_stn_clean_count=65;

}
