package in.spice.railway.feedback;

public class ObdRowPojo {

	String pnr, mobileNumber, trainNo, boardingStation, reservedUpto, coachClass, finalStatus, callingTime, departTime,
			obdPromt, obdCurStatus, doj, lang, coachNo, seatNo;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(String trainNo) {
		this.trainNo = trainNo;
	}

	public String getBoardingStation() {
		return boardingStation;
	}

	public void setBoardingStation(String boardingStation) {
		this.boardingStation = boardingStation;
	}

	public String getReservedUpto() {
		return reservedUpto;
	}

	public void setReservedUpto(String reservedUpto) {
		this.reservedUpto = reservedUpto;
	}

	public String getCoachClass() {
		return coachClass;
	}

	public void setCoachClass(String coachClass) {
		this.coachClass = coachClass;
	}

	public String getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}

	public String getCallingTime() {
		return callingTime;
	}

	public void setCallingTime(String callingTime) {
		this.callingTime = callingTime;
	}

	public String getDepartTime() {
		return departTime;
	}

	public void setDepartTime(String departTime) {
		this.departTime = departTime;
	}

	public String getObdPromt() {
		return obdPromt;
	}

	public void setObdPromt(String obdPromt) {
		this.obdPromt = obdPromt;
	}

	public String getObdCurStatus() {
		return obdCurStatus;
	}

	public void setObdCurStatus(String obdCurStatus) {
		this.obdCurStatus = obdCurStatus;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCoachNo() {
		return coachNo;
	}

	public void setCoachNo(String coachNo) {
		this.coachNo = coachNo;
	}

	public String getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}

}
