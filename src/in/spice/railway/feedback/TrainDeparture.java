package in.spice.railway.feedback;

import in.spice.railway.feedback.db.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

public class TrainDeparture {

	public static Map<TrainStation, String> insertIntoMap() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<TrainStation, String> trainStationMap = new HashMap<TrainStation, String>();
		try {
			connection = DatabaseManager.getConnection();
			stmt = connection
					.prepareStatement("select * from train_station_departure_testing");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String train = rs.getString("train_number");
				String station = rs.getString("stn_code");
				String departure = rs.getString("depart_time");
				TrainStation trainStation = new TrainStation();
				trainStation.setTrain(train);
				trainStation.setStation(station.trim());
				trainStationMap.put(trainStation, departure);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}
		return trainStationMap;
	}
}
