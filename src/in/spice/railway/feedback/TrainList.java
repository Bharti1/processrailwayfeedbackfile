package in.spice.railway.feedback;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;

import in.spice.railway.feedback.db.DatabaseManager;

public class TrainList {
	
//	public static List<String> trainList = new ArrayList<String>();
	
	public static Set<String> trainList = new HashSet<String>();

	public static Set<String> getTrainList() {
		return trainList;
	}

	public static void setTrainList(Set<String> trainList) {
		TrainList.trainList = trainList;
	}

	public static Set<String> insertIntoList() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("select train_no from obd_lang");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String trainNumber = rs.getString(1);
				trainList.add(trainNumber);
//				String lang = rs.getString("lang");
//				obdLangMap.put(trainNumber, lang);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}
		return trainList;
	}
	
	
	public static List<String> selectedTrainList(){

		Properties prop = new Properties();
		InputStream input = null;
		List<String> sortedTrainList=new ArrayList<String>();

		try {

			String filename = "G:\\ProcessRailwayOBDData\\trainsList_new.properties";
			input = new FileInputStream(filename);
			prop.load(input);
			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				sortedTrainList.add(key);
			}
			System.out.println("Train list size ::::" + sortedTrainList.size());

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sortedTrainList;
	}
	
	
	public static List<String> selectedStationCode(){

		Properties prop = new Properties();
		InputStream input = null;
		List<String> sortedTrainList=new ArrayList<String>();

		try {

			String filename = "G:\\ProcessRailwayOBDData\\stationCode_new.properties";
			input = new FileInputStream(filename);
			prop.load(input);
			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				sortedTrainList.add(key);
			}
			System.out.println("Station code list size ::::" + sortedTrainList.size());

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sortedTrainList;
	}
}
