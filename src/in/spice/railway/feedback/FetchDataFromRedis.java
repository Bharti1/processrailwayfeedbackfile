package in.spice.railway.feedback;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class FetchDataFromRedis
{
  public static JedisPool jedisPool = null;
  public FetchDataFromRedis() {}

  public long readTrainCounterRedis(String type, String train_no, Jedis jedis, JedisPoolConfig poolConfig)
  {
    long count = 0L;

    try
    {
      if ((type != null) && (type.equals("0"))) {
        count = jedis.scard("0_" + train_no).longValue();
      }
      else if ((type != null) && (type.equals("2"))) {
        count = jedis.scard("2_" + train_no).longValue();
      }
      else if ((type != null) && (type.equals("1"))) {
        count = jedis.scard("1_" + train_no).longValue();

      }

    }
    catch (Exception e)
    {

      count = 0L;
     System.err.println("Error while connecting to jedis:::::::::::::::"+e.getMessage());
    }
    return count;
  }
  


  public long readStationCounterRedis(String type, String station_no, Jedis jedis, JedisPoolConfig poolConfig)
  {
    long count = 0L;

    try
    {
      if ((type != null) && (type.equals("1"))) {
        count = jedis.scard("1_" + station_no).longValue();

      }

    }
    catch (Exception e)
    {

      count = 0L;
      System.err.println("Error while connecting to jedis:::::::::::::::"+e.getMessage());
    }
    return count;
  }
  
  public String checkMsisdn(String msisdn) {
    if (!msisdn.startsWith("0")) {
      return "0" + msisdn;
    }
    return msisdn;
  }
  
  private JedisPoolConfig buildPoolConfig() {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    
    poolConfig.setMaxIdle(65);
    poolConfig.setMinIdle(16);
    poolConfig.setTestOnBorrow(true);
    poolConfig.setTestOnReturn(true);
    poolConfig.setTestWhileIdle(true);
    poolConfig.setMinEvictableIdleTimeMillis(60000L);
    poolConfig.setTimeBetweenEvictionRunsMillis(30000L);
    poolConfig.setNumTestsPerEvictionRun(3);
    
    return poolConfig;
  }
  
  public static void main(String[] args) {}
}