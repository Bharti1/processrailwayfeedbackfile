package in.spice.railway.feedback;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author CH-E00246
 */
public class Alert {

	public static void main(String[] args) {
		Alert sendSms = new Alert();
		sendSms.getSendSmsAlert("Testing");
	}

	public void getSendSmsAlert(String strMessage) {
		String strUrl = "";
//		ArrayList<String> mobileNumbers = new ArrayList<String>();

		String mobileNos = "919219482378,919814330940";
		String[] arrMobile = mobileNos.split(",");

		List<String> mobileNumbers = Arrays.asList(arrMobile);
//		for (String str : arrMobile) {
//			MobileNumbers.add(str);
//		}

		// MobileNumbers.add("9216346405");
		
		// getting exception for feedback calling data
		try{
		for (String mobileNumber : mobileNumbers) {
//			String encodedStr = URLEncoder.encode("ani=91"+ mobileNumber+ "&uname=demotrans&passwd=dem@324&cli=SMSSDL&message="+ strMessage, "UTF-8");
//			strUrl = "http://103.15.179.45:8085/BulkMessaging/sendingSMS?"+encodedStr;
			//strUrl = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=McomOTP&Password=McomOTP&MessageType=txt&Mobile="+mobileNumber+"&SenderID=sFreeB&Message="+strMessage;
			strUrl = "http://103.15.179.45:8085/BulkMessaging/sendingSMS?uname=Demotrans&passwd=dem@324&cli=RBFDBK&ani="+mobileNumber+"&message="+strMessage;
			try {
				sendMessage(strUrl);
			} catch (Exception e) {
				e.printStackTrace();
			}

			}
		} catch(Exception e){
			e.printStackTrace();
		}

	}


	private String sendMessage(String strUrl) {

		BufferedReader bufferedReader = null;
		HttpURLConnection connection = null;
		InputStream is = null;
		URL url;
		String strOutPut = "", strLogString = "";
		String strInputLine = "";
		DataOutputStream wr = null;
		try {
			strUrl = strUrl.replaceAll(" ", "%20");
			url = new URL(strUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setReadTimeout(5000);
			connection.setConnectTimeout(10000);

			wr = new DataOutputStream(connection.getOutputStream());

			wr.writeBytes(strLogString);

			is = connection.getInputStream();
			bufferedReader = new BufferedReader(new InputStreamReader(is));
			while ((strInputLine = bufferedReader.readLine()) != null) {
				strOutPut += strInputLine;
			}
			System.out.println("Response XML:" + strOutPut);
		} catch (Exception e) {
			strOutPut = "";
			e.printStackTrace();

		} finally {
			if (wr != null) {
				try {
					wr.flush();
					wr.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return strOutPut;
	}
}
