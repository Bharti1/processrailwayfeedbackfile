package in.spice.railway.feedback;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utilities {
	private static Properties objProperties;

	public static String getConfKeyValue(String key) throws IOException {
		FileInputStream fis = null;
		try {
			String filePath = "";
			if (objProperties == null) {
				objProperties = new Properties();
				filePath = "E:/IpConfig.properties";
				fis = new FileInputStream(filePath);
				objProperties.load(fis);
			}
		} catch (Exception e) {
			System.out.println("Error while loading Property file:" + e.getMessage());
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
		return objProperties.getProperty(key);
	}

}
