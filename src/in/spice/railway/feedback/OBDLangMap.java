package in.spice.railway.feedback;

import in.spice.railway.feedback.db.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

public class OBDLangMap {

	public static Map<String, String> insertIntoMap() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<String, String> obdLangMap = new HashMap<String, String>();
		try {
			connection = DatabaseManager.getConnection();
			stmt = connection.prepareStatement("select train_no,lang from obd_lang_testing");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String trainNumber = rs.getString("train_no");
				String lang = rs.getString("lang");
				obdLangMap.put(trainNumber, lang);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}
		return obdLangMap;
	}
}
