package in.spice.railway.feedback;

import in.spice.railway.feedback.db.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

public class PnrList {

	public static List<String> listOfPnrNumbers() {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		List<String> pnrNumbers = new ArrayList<String>();

		try {
			connection = DatabaseManager.getConnection();
			stmt = connection
					.prepareStatement("select pnr_number from tbl_outdial_testing");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String pnrNumber = rs.getString(1);
				pnrNumbers.add(pnrNumber);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(connection);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
		}

		return pnrNumbers;
	}

}
