package in.spice.railway.feedback.db;

import in.spice.railway.feedback.config.ConfigurationManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseManager {

	public static Connection objConnection_ora = null;

	public static Connection getConnection() throws SQLException {
		ConfigurationManager cm = ConfigurationManager.getInstance();
		Properties p = cm.getProperties();

		String username = p.getProperty("database.username");
		String password = p.getProperty("database.password");
		String jdbcUrl = p.getProperty("database.jdbcUrl");
		String jdbcDriver = p.getProperty("database.jdbcDriver");

		try {
			Class.forName(jdbcDriver);
		} catch (ClassNotFoundException e) {
			throw new SQLException(e);
		}

		return DriverManager.getConnection(jdbcUrl, username, password);
	}

//	public static Connection getConnection_ora() {
//		try {
//			if (objConnection_ora == null || objConnection_ora.isClosed()) {
//				DriverManager
//						.registerDriver(new oracle.jdbc.driver.OracleDriver());
//				// System.out.println("Connection is");
//				objConnection_ora = DriverManager.getConnection(
//						"jdbc:oracle:thin:@192.168.9.140:1521:DND", "dnd",
//						"dnd");
//				// System.out.println("Connection is 135 "+objConnection);
//			}
//		} catch (Exception objException) {
//			System.out.println("Unable to make connection*********");
//			objException.printStackTrace();
//		}
//		return objConnection_ora;
//	}
}