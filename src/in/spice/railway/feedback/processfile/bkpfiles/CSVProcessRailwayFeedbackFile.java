package in.spice.railway.feedback.processfile.bkpfiles;

import in.spice.railway.feedback.CallTimePojo;
import in.spice.railway.feedback.OBDLangMap;
import in.spice.railway.feedback.PnrList;
import in.spice.railway.feedback.TrainOBD;
import in.spice.railway.feedback.TrainOBDMap;
import in.spice.railway.feedback.db.DatabaseManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;

public class CSVProcessRailwayFeedbackFile {

	public static void main(String[] args) throws IOException {
		
		long createMapTime = System.currentTimeMillis();

		Map<TrainOBD, String> obdMap = TrainOBDMap.insertIntoMap("0");
		Map<String, String> obdLangMap = OBDLangMap.insertIntoMap();

		File dir = new File("E:\\07072015\\07072015\\");

		File[] dirListing = dir.listFiles();

		System.out.println("size of directory " + dirListing.length);

		// String fileLimit = args[0];
		String fileLimit = "100";

		int fileLimitInt = Integer.parseInt(fileLimit);
		int limitOfFiles = fileLimitInt - 1;

		float fileCounter = (float) dirListing.length / limitOfFiles;
		int files = (int) Math.ceil(fileCounter);
		if (files == 0) {
			files = 1;
		}

		FileInputStream in = null;

		int fileCount = 0;
		//
		long createPnrList = System.currentTimeMillis();
		List<String> pnrList = PnrList.listOfPnrNumbers();
		System.out.println("time to get pnr list "
				+ (System.currentTimeMillis() - createPnrList) + ", size="
				+ pnrList.size());
		Connection conn = null;
		PreparedStatement st = null;

		List<File> fileNames = new ArrayList<File>();
		try {
			conn = DatabaseManager.getConnection();
			st = conn
					.prepareStatement("LOAD DATA LOCAL INFILE ? INTO TABLE tbl_outdial_csv_test FIELDS TERMINATED BY ',' "
							+ "LINES TERMINATED BY '\n' (pnr_number,msisdn,train_no,station_code,class,calling_time,obd,"
							+ "obd_cur_status,lang,insert_datetime,STATUS,doj,depart_time,reserved_upto,final_status);");

			// String csvFilename = "E:\\CSVTEST.csv";
			String csvFilename = "E:\\csv\\csv_" + uniqueId() + ".csv";
			FileWriter out = new FileWriter(csvFilename);
			CSVFormat csvf = CSVFormat.MYSQL.withDelimiter(',');
			CSVPrinter csvp = new CSVPrinter(out, csvf);
			// FileWriter out = null;
			// CSVFormat csvf = CSVFormat.MYSQL.withDelimiter(',');
			// CSVPrinter csvp = null;
			// FileWriter out = null;
			// CSVFormat csvf = CSVFormat.MYSQL.withDelimiter(',');
			// CSVPrinter csvp = null;
			int j = 0, l = 0, f = 0;

			// for (File f : dirListing) {
			for (int k = 1; k <= dirListing.length; k++) {
				// in = new FileInputStream(f);
				if(dirListing[k-1].isFile()){
				in = new FileInputStream(dirListing[k - 1]);
				++fileCount;
				++j;
				// int totalLines = 0;
				// int addedLines = 0;

				if (k % 100 == 0) {
					l = j;
					j = 0;
				}

				if (l == 100 && j == 0 && dirListing.length != limitOfFiles) {
					++f;
					csvFilename = "E:\\csv\\csv_" + f + "_" + uniqueId()
							+ ".csv";
					out = new FileWriter(csvFilename);
					csvp = new CSVPrinter(out, csvf);
					l = 0;
				}
				/*
				 * else if ((files - 1) == f) { csvFilename = "E:\\csv\\csv_" +
				 * f + "_" + uniqueId() + ".csv"; out = new
				 * FileWriter(csvFilename); csvp = new CSVPrinter(out, csvf);
				 * ++f; }
				 */

				List<String> lines = IOUtils.readLines(in);

				// totalLines = lines.size();

				String dateStr = lines.get(0).split(",")[3].split(":")[0];
				String timeStr = lines.get(0).split(",")[3].split(":")[1];
				String departTime = lines.get(0).split(",")[1].split(":")[1];

				CallTimePojo pojo = getCallDateTime(dateStr, timeStr);

				for (int i = 1; i < lines.size(); i++) {
					List<String> formattedLine = new ArrayList<String>();

					String[] parts = lines.get(i).split(",");

					if (parts.length != 35) {
						continue;
					}

					String pnr = parts[2].trim();
					String mobileNumber = parts[3].trim();
					String trainNumber = parts[4].trim();
					String dojDay = parts[5].trim();
					String dojMonth = parts[6].trim();
					String boardingStation = parts[7].trim();
					String reservedUpto = parts[8].trim();
					String coachClass = parts[11].trim();
					// String bookingStatus = parts[13].trim();
					String finalStatus = parts[17].trim();
					String callTime = pojo.getCallTime();
					String obdStatus = pojo.getObdCurrentStatus();

					String Year = getYear(dojDay + dojMonth);
					String doj = Year + "-" + dojMonth + "-" + dojDay;

					TrainOBD obd = new TrainOBD();
					obd.setTrainClass(coachClass);
					obd.setTrainNumber(trainNumber);

					String lang = obdLangMap.get(trainNumber);

					String obdPromt = obdMap.get(obd);

					if (obdPromt == null || "".equals(obdPromt)) {
						obdPromt = "0";
					}

					if (lang == null || "".equals(lang)) {
						lang = "h";
					}

					if (mobileNumber.length() != 10
							|| (!"CNF".equalsIgnoreCase(finalStatus) && !"RAC"
									.equalsIgnoreCase(finalStatus))) {
						continue;
					}
					if (!pnrList.contains(pnr)) {
						pnrList.add(pnr);
						SimpleDateFormat formatter = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");

						formattedLine.add(pnr);
						formattedLine.add(mobileNumber);
						formattedLine.add(trainNumber);
						formattedLine.add(boardingStation);
						formattedLine.add(coachClass);
						formattedLine.add(callTime);
						formattedLine.add(obdPromt);
						formattedLine.add(obdStatus);
						formattedLine.add(lang);
						formattedLine.add(formatter.format(new Date()));
						formattedLine.add("I");
						formattedLine.add(doj);
						formattedLine.add(departTime);
						formattedLine.add(reservedUpto);
						formattedLine.add(finalStatus);

						csvp.printRecord(formattedLine);
						// ++addedLines;

					}
				}
				fileNames.add(dirListing[k - 1]);
				if (fileCount % limitOfFiles == 0) {
					st.setString(1, csvFilename);
					st.execute();
				}

				// System.out.printf("%s,%d,%d\n", f.getName(), totalLines,
				// addedLines);
				System.out.println("fileCount is " + fileCount);
				IOUtils.closeQuietly(in);
			}
			}

			if (!"".equals(csvFilename)) {
				st.setString(1, csvFilename);
				st.execute();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(conn);
		}

		for (File f : fileNames) {
			try {
				String doneFilesFolder = "E:\\07072015\\07072015\\done"
						+ getCurrentDate();
				File doneFolder = new File(doneFilesFolder);
				if (!doneFolder.isDirectory() && !doneFolder.exists()) {
					doneFolder.mkdir();
				}

				String doneFiles = doneFilesFolder + "\\" + f.getName();

				File destFile = new File(doneFiles);

				if (f.renameTo(destFile)) {
					System.out.println("File is moved successful!");
				} else {
					System.out.println("File is failed to move!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("final time of complete process "+(System.currentTimeMillis() - createMapTime));
	}

	public static CallTimePojo getCallDateTime(String date, String time) {
		SimpleDateFormat inputDateFormat = new SimpleDateFormat(
				"ddMMyyyy HH:mm");

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date d = null;

		String h = time.substring(0, 2);
		String m = time.substring(2, 4);
		time = h + ":" + m;
		try {
			d = inputDateFormat.parse(String.format("%s %s", date, time));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(d);

		Calendar c = Calendar.getInstance();
		c.setTime(d);

		c.add(Calendar.HOUR, 4);

		// System.out.println(c.getTime());

		int hour = c.get(Calendar.HOUR_OF_DAY);
		// int min = c.get(Calendar.MINUTE);
		String obdStatus = "";
		if (hour >= 21 && hour <= 23) {
			c.add(Calendar.DATE, 1);
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else if (hour < 9) {
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else {
			obdStatus = "current";
		}
		Date finalDate = new Date(c.getTimeInMillis());
		String finalDateTime = formatter.format(finalDate);
		CallTimePojo callTimePojo = new CallTimePojo();
		callTimePojo.setCallTime(finalDateTime);
		callTimePojo.setObdCurrentStatus(obdStatus);
		return callTimePojo;
	}

	public static String getYear(String strDDMM) {
		String str_inmonth = strDDMM.substring(2);
		int in_month = Integer.parseInt(str_inmonth);
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(2) + 1;
		int year = calendar.get(1);
		if (in_month < month) {
			year = year + 1;
		}
		String strYear = Integer.toString(year);
		String str = strYear;
		return str;
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		return format.format(date);
	}

	public static long uniqueId() {
		return System.currentTimeMillis();
	}
}
