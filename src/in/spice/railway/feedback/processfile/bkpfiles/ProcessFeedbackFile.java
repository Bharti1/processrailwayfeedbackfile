package in.spice.railway.feedback.processfile.bkpfiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;

import in.spice.railway.feedback.Alert;
import in.spice.railway.feedback.CallTimePojo;
import in.spice.railway.feedback.OBDLangMap;
import in.spice.railway.feedback.PnrList;
import in.spice.railway.feedback.TrainDeparture;
import in.spice.railway.feedback.TrainOBD;
import in.spice.railway.feedback.TrainOBDMap;
import in.spice.railway.feedback.TrainStation;
import in.spice.railway.feedback.Utilities;
import in.spice.railway.feedback.config.Log;
import in.spice.railway.feedback.db.DatabaseManager;

public class ProcessFeedbackFile {

	public static void main(String[] args) throws IOException {

		// File lockFile = new File("G:\\ProcessRailwayOBDData\\process.lck");
		//
		// if(lockFile.exists()){
		// System.exit(0);
		// } else{
		// lockFile.createNewFile();
		// }

		String processLine;
		try {
			Process proc = Runtime.getRuntime().exec("WMIC path win32_process get Caption,Processid,Commandline");
			BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			OutputStreamWriter oStream = new OutputStreamWriter(proc.getOutputStream());
			// oStream.write("process where name='ProcessRailwayFile.jar'");
			oStream.flush();
			oStream.close();
			int i = 0;
			while ((processLine = input.readLine()) != null) {
				if (processLine.contains("ProcessRailwayFile.jar")) {
					++i;
					if (i > 1) {
						processLine = processLine.replaceAll("\\D", "");
						processLine = processLine.substring(4, processLine.length());
						System.out.println("processLine id is " + processLine);
						String currentProcessId = ManagementFactory.getRuntimeMXBean().getName();
						System.out.println("original currentProcessId " + currentProcessId);
						currentProcessId = currentProcessId.split("@")[0];
						System.out.println("currentProcessId is " + currentProcessId);
						if (processLine.equals(currentProcessId)) {
							System.out.println("multiple processes");
							System.exit(0);
						}
					}
					System.out.println("first process id of manag " + ManagementFactory.getRuntimeMXBean().getName());
					System.out.println("Single process.");
					// System.out.println("Process exited" + processLine);
					// System.exit(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		long createMapTime = System.currentTimeMillis();
		Map<TrainOBD, String> obdMap = TrainOBDMap.insertIntoMap("0");
		Map<String, String> obdLangMap = OBDLangMap.insertIntoMap();
		Map<TrainStation, String> trainDepartureMap = TrainDeparture.insertIntoMap();

		Log.getTimeLogs("time to create maps " + (System.currentTimeMillis() - createMapTime));

		long createPnrList = System.currentTimeMillis();
		List<String> pnrList = PnrList.listOfPnrNumbers();
		if (pnrList == null) {
			pnrList = new ArrayList<String>();
		}
		System.out.println("pnr list size is " + pnrList.size());
		Log.getTimeLogs(
				"time to create pnr List " + (System.currentTimeMillis() - createPnrList) + ", size=" + pnrList.size());

		final int batchSize = 700;

		String fileName = "";
		String alertPnr = "";

		String dirName = "E:\\139_Voice_CDR\\";
		// String dirName = "E:\\07072015\\07072015\\";
		// String dirName = "E:\\Data_9\\Data\\";
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");

		File dir = new File(dirName);

		File[] dirListing = dir.listFiles();

		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DatabaseManager.getConnection();

			st = conn.prepareStatement("INSERT INTO tbl_outdial_testing (insert_datetime,pnr_number,msisdn,train_no,"
					+ "station_code,reserved_upto,class,final_status"
					+ ",calling_time,depart_time,obd,obd_cur_status,doj,lang,status,coach,seat_number)"
					+ " values (now(),?,?,?,?,?,?,?,?,?,?,?,?,?,'I',?,?)");

			for (File f : dirListing) {
				if (f.isFile() && f.length() > 0) {
					String trainNumber = f.getName().substring(0, 5);
					if (obdLangMap.containsKey(trainNumber)) {
						FileReader in = new FileReader(f);
						List<String> lines = IOUtils.readLines(in);

						fileName = f.getName();

						String[] firstLine = lines.get(0).split(",");

						String dateStr = firstLine[3].split(":")[0];
						String timeStr = firstLine[3].split(":")[1];
						String departTime = firstLine[1].split(":")[1];

						Date d = sdf.parse(timeStr);
						// CallTimePojo pojo = getCallDateTime(dateStr,
						// timeStr);
						// String callingTime = pojo.getCallTime();
						// String obdCurStatus = pojo.getObdCurrentStatus();

						Calendar c1 = Calendar.getInstance();
						c1.setTime(d);

						c1.add(Calendar.HOUR, 4);

						int hour = c1.get(Calendar.HOUR);

						int validLines = 0;
						int invalidLines = 0;
						int dupPnrs = 0;

						int count = 0;
						long insertRecordTime = System.currentTimeMillis();
						for (int i = 1; i < lines.size(); i++) {
							String line = lines.get(i);

							String[] lineArr = line.split(",");

							/*
							 * if (lineArr.length != 35) { ++invalidLines;
							 * continue; }
							 */

							String pnr = lineArr[2].trim();
							String mobileNumber = lineArr[3].trim();
							String finalStatus = lineArr[17].trim();

							alertPnr = pnr;

							if (mobileNumber.length() != 10
									|| (!"CNF".equalsIgnoreCase(finalStatus) && !"RAC".equalsIgnoreCase(finalStatus))) {
								++invalidLines;
								continue;
							} else {
								long checkPnrTime = System.currentTimeMillis();
								if (!pnrList.contains(pnr)) {
									Log.getTimeLogs("time to check in pnr List in if "
											+ (System.currentTimeMillis() - checkPnrTime) + ", of size="
											+ pnrList.size());

									pnrList.add(pnr);
									String trainNo = lineArr[4].trim();
									String boardingStation = lineArr[7].trim();
									String reservedUpto = lineArr[8].trim();
									String dojDay = lineArr[5].trim();
									String dojMonth = lineArr[6].trim();
									String coachClass = lineArr[11].trim();
									String year = getYear(dojDay + dojMonth);
									String doj = year + "-" + dojMonth + "-" + dojDay;
									String coachNo = lineArr[18].trim();
									String seatNo = lineArr[19].trim();
									TrainOBD obd = new TrainOBD();
									obd.setTrainClass(coachClass);
									obd.setTrainNumber(trainNumber);

									TrainStation ts = new TrainStation();
									ts.setStation(boardingStation);
									ts.setTrain(trainNo);

									String trainDepartTime = trainDepartureMap.get(ts);

									if (trainDepartTime == null || "".equals(trainDepartTime)) {

										String h = timeStr.substring(0, 2);
										String m = timeStr.substring(2, 4);
										trainDepartTime = h + ":" + m;
									}

									String dojForCalling = dojDay + dojMonth + year;

									String lang = obdLangMap.get(trainNumber);

									String obdPromt = obdMap.get(obd);

									if (obdPromt == null || "".equals(obdPromt)) {
										obdPromt = "0";
									}

									if (lang == null || "".equals(lang)) {
										lang = "h";
									}
									int addHour = 3;

									CallTimePojo pojo = null;

									if ("1".equals(obdPromt)) {
										addHour = 4;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour);
										
									} else if ("2".equals(obdPromt)) {
//										pojo = getCallDateTimeForObd2(dojForCalling, year, boardingStation, trainNo,
//												fileName, trainDepartTime, dojDay, dojMonth);
										addHour = 7;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour);
									} else {
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour);
									}
									
									if(pojo==null){
										continue;
									}

									String callingTime = pojo.getCallTime();
									String obdCurStatus = pojo.getObdCurrentStatus();

									st.setString(1, pnr);
									st.setString(2, mobileNumber);
									st.setString(3, trainNo);
									st.setString(4, boardingStation);
									st.setString(5, reservedUpto);
									st.setString(6, coachClass);
									st.setString(7, finalStatus);
									st.setString(8, callingTime);
									st.setString(9, departTime);
									st.setString(10, obdPromt);
									st.setString(11, obdCurStatus);
									st.setString(12, doj);
									st.setString(13, lang);
									st.setString(14, coachNo);
									st.setString(15, seatNo);
									st.addBatch();

									if (++count % batchSize == 0) {
										st.executeBatch();
										System.out.println("batch uploaded");
										st.clearParameters();
									}

									++validLines;

								} else {
									Log.getTimeLogs("time to check in pnr List in else "
											+ (System.currentTimeMillis() - checkPnrTime) + ", of size="
											+ pnrList.size());
									++dupPnrs;
								}
							}
						}

						Log.getTimeLogs("Total time to insert and check pnr in list " + validLines + " record of file ="
								+ f.getName() + "--->" + (System.currentTimeMillis() - insertRecordTime));

						st.executeBatch();
						st.clearParameters();

						Log.getProcessLogs(f.getName() + "," + hour + "," + lines.size() + "," + invalidLines + ","
								+ validLines + "," + dupPnrs);

						System.out.println(f.getName() + "," + hour + "," + lines.size() + "," + invalidLines + ","
								+ validLines + "," + dupPnrs);
						IOUtils.closeQuietly(in);

						long timeToMoveFile = System.currentTimeMillis();
						String doneFilesFolder = "E:\\139_Voice_CDR\\done" + getCurrentDate();
						// String doneFilesFolder =
						// "E:\\07072015\\07072015\\done"
						// + getCurrentDate();

						// String doneFilesFolder =
						// "E:\\data\\data\\done"+getCurrentDate();
						File doneFolder = new File(doneFilesFolder);
						if (!doneFolder.isDirectory() && !doneFolder.exists()) {
							doneFolder.mkdir();
						}

						// String Filepath = "E:\\139_Voice_CDR\\" +
						// f.getName();
						// String Filepath = "E:\\data\\data\\new\\" +
						// f.getName();
						String doneFiles = doneFilesFolder + "\\" + f.getName();
						f.delete();
						Thread.sleep(10);

						// File srcFile = new File(Filepath);
						File destFile = new File(doneFiles);

						FileOutputStream fos = new FileOutputStream(destFile);
						IOUtils.writeLines(lines, "\n", fos);
						IOUtils.closeQuietly(fos);

						Log.getTimeLogs("time to move file --->" + (System.currentTimeMillis() - timeToMoveFile));
					}

				}

			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			Log.getErrorLogs(fileName, "exception in first catch file not found----------> for pnr =" + alertPnr
					+ " exception is= " + e1.getMessage());
			new Alert().getSendSmsAlert("In Filename " + fileName + ",first catch for pnr " + alertPnr
					+ ", exception is " + e1.getMessage() + ", at time " + new Date());
		} catch (SQLException e2) {
			e2.printStackTrace();
			Log.getErrorLogs(fileName, "exception in second catch sql----------> for pnr =" + alertPnr
					+ " exception is= " + e2.getMessage());
			new Alert().getSendSmsAlert("In Filename " + fileName + ",second catch for pnr " + alertPnr
					+ ", exception is " + e2.getMessage() + ", at time " + new Date());
		} catch (Exception e) {
			e.printStackTrace();
			Log.getErrorLogs(fileName, "exception in third catch normal exception----------> for pnr =" + alertPnr
					+ " exception is= " + e.getMessage());
			new Alert().getSendSmsAlert("In Filename " + fileName + ",third catch for pnr " + alertPnr
					+ ", exception is " + e.getMessage() + ", at time " + new Date());
		} finally {
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(conn);
			// lockFile.delete();
		}
		System.out.println("final time of complete process " + (System.currentTimeMillis() - createMapTime));
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		return format.format(date);
	}

	public static CallTimePojo getCallDateTimeForObd2(String date, String year, String station, String train,
			String fileName, String fileTime, String dojDay, String dojMonth) throws Exception {
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("ddMMyyyy HH:mm");

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date d = null;

		if (year != null && year.length() == 4) {
			year = year.substring(2, 4);
		}

		String response = hitUrl(train, year, station, fileName, dojDay, dojMonth);
		String time = getTime(response);

		try {
			if (time != null && !"".equals(time)) {
				d = inputDateFormat.parse(String.format("%s %s", date, time));
			} else {
				d = inputDateFormat.parse(String.format("%s %s", date, fileTime));
			}
		} catch (Exception e) {
			Log.getErrorLogs(fileName, "error  in date parsing " + e.getMessage());
			e.printStackTrace();
		}
		// System.out.println(d);

		Calendar c = Calendar.getInstance();
		c.setTime(d);

		if (time == null || "".equals(time)) {
			c.add(Calendar.HOUR, 5);
		} else {
			c.add(Calendar.HOUR, 3);
		}
		// System.out.println(c.getTime());

		int hour = c.get(Calendar.HOUR_OF_DAY);
		// int min = c.get(Calendar.MINUTE);
		String obdStatus = "";
		if (hour >= 21 && hour <= 23) {
			c.add(Calendar.DATE, 1);
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else if (hour < 9) {
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else {
			obdStatus = "current";
		}
		
		if(obdStatus.equalsIgnoreCase("backlog")){
			return null;
		}
		Date finalDate = new Date(c.getTimeInMillis());
		String finalDateTime = formatter.format(finalDate);
		CallTimePojo callTimePojo = new CallTimePojo();
		callTimePojo.setCallTime(finalDateTime);
		callTimePojo.setObdCurrentStatus(obdStatus);
		return callTimePojo;
	}

	public static CallTimePojo getCallDateTime(String date, String time, int addHour) {
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("ddMMyyyy HH:mm");

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		// SimpleDateFormat formatter = new
		// SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d = null;

		// String h = time.substring(0, 2);
		// String m = time.substring(2, 4);
		// time = h + ":" + m;
		try {
			d = inputDateFormat.parse(String.format("%s %s", date, time));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(d);

		Calendar c = Calendar.getInstance();
		c.setTime(d);

		c.add(Calendar.HOUR, addHour);

		// System.out.println(c.getTime());

		int hour = c.get(Calendar.HOUR_OF_DAY);
		// int min = c.get(Calendar.MINUTE);
		String obdStatus = "";
		if (hour >= 21 && hour <= 23) {
			c.add(Calendar.DATE, 1);
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else if (hour < 9) {
			c.set(Calendar.HOUR_OF_DAY, 9);
			c.set(Calendar.MINUTE, 0);
			obdStatus = "backlog";
		} else {
			obdStatus = "current";
		}
		
		if(obdStatus.equalsIgnoreCase("backlog")){
			return null;
		}
		Date finalDate = new Date(c.getTimeInMillis());
		String finalDateTime = formatter.format(finalDate);
		CallTimePojo callTimePojo = new CallTimePojo();
		callTimePojo.setCallTime(finalDateTime);
		callTimePojo.setObdCurrentStatus(obdStatus);
		return callTimePojo;
	}

	public static String getYear(String strDDMM) {
		String str_inmonth = strDDMM.substring(2);
		int in_month = Integer.parseInt(str_inmonth);
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(2) + 1;
		int year = calendar.get(1);
		if (in_month < month) {
			year = year + 1;
		}
		String strYear = Integer.toString(year);
		String str = strYear;
		return str;
	}

	public static String hitUrl(String trainNUmber, String year, String stationCode, String fileName, String dojDay,
			String dojMonth) throws Exception {

		String ip = Utilities.getConfKeyValue("ip");

		String doj = dojDay + dojMonth + year;

		String url = "http://" + ip + "/ArrivalDeparture/trainrequest.aspx?trnno=%s&doj=%s&stdcode=%s";
		url = String.format(url, trainNUmber, doj, stationCode);
		Log.getProcessLogs("url is:- " + url);
		InputStream in = null;
		InputStreamReader reader = null;
		BufferedReader br = null;
		String resp = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setConnectTimeout(3000);
			con.setReadTimeout(3000);

			// int responseCode = con.getResponseCode();

			// System.out.println("\nSending 'GET' request to URL : " + url);
			// System.out.println("Response Code : " + responseCode);

			in = con.getInputStream();
			reader = new InputStreamReader(in);
			br = new BufferedReader(reader);
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
			}
			resp = response.toString();
			System.out.println("response of url is " + resp);
			Log.getProcessLogs("reponse of url:- " + url + " is -" + resp);
		} catch (Exception e) {
			Log.getErrorLogs(fileName, "error is " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
			if (reader != null) {
				reader.close();
			}
			if (br != null) {
				br.close();
			}
		}
		return resp;

	}

	static String getTime(String apiResponse) {
		String resp = "";

		if (apiResponse != null && apiResponse.contains("#")) {
			String[] response = apiResponse.split("#");
			if (response != null && response.length >= 4) {
				String expectedArrival = response[4];
				if (expectedArrival != null && expectedArrival.contains(":")) {
					expectedArrival = expectedArrival.substring(expectedArrival.indexOf(":"));
					if (expectedArrival.contains(",")) {
						String[] times = expectedArrival.split(",");
						if (times != null && times.length > 1) {
							resp = times[1].trim();
						}
					}
				}
			}
		}
		return resp;

	}
}