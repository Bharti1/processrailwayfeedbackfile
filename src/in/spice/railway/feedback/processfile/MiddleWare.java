package in.spice.railway.feedback.processfile;

import in.spice.railway.feedback.Alert;
import in.spice.railway.feedback.CallTimePojo;
import in.spice.railway.feedback.FetchDataFromRedis;
import in.spice.railway.feedback.ObdRowPojo;
import in.spice.railway.feedback.PnrList;
import in.spice.railway.feedback.TrainOBD;
import in.spice.railway.feedback.TrainStation;
import in.spice.railway.feedback.UpdateObdOutdial;
import in.spice.railway.feedback.Utilities;
import in.spice.railway.feedback.config.Log;
import in.spice.railway.feedback.db.DatabaseManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


public class MiddleWare
implements Runnable
{
	private Map<TrainOBD, String> obdTrainMap = new HashMap<TrainOBD, String>();
	private List<File> filesList = new ArrayList<File>();
	private String fileName = "";
	private Map<TrainOBD, String> obdBedRollMap = new HashMap<TrainOBD, String>();
	private Map<String, String> obdLangMap = new HashMap<String, String>();
	private Map<TrainStation, String> trainDepartureMap = new HashMap<TrainStation, String>();
	private List<String> trainList = new ArrayList<String>();
	private String[] classBedRoll = null;
	private List<String> classBedRollList = new ArrayList<String>();
	private SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	private int batchSize = 0;
	private String alertPnr;
	private List<String> pnrList = new ArrayList<String>();
	private List<String> sortedStationCodeList = new ArrayList<String>();
	private List<String> sortedTrainList = new ArrayList<String>();
	private long createMapTime = 0L;
	private int daysBack = 1;
	private FetchDataFromRedis countFromRedis = new FetchDataFromRedis();
	private static int obdCounter = 0;
	private static boolean obdCounterFlag = false;
	private static int totalProcessedCount = 0;


	private JedisPoolConfig poolConfig;


	public MiddleWare(List<File> filesList, Map<TrainOBD, String> obdTrainMap, Map<TrainOBD, String> obdBedRollMap, Map<String, String> obdLangMap, Map<TrainStation, String> trainDepartureMap, List<String> trainList, String[] classBedRoll, List<String> classBedRollList, SimpleDateFormat sdf, int batchSize, String alertPnr, List<String> pnrList, List<String> sortedStationCodeList, List<String> sortedTrainList, long createMapTime, JedisPoolConfig poolConfig)
	{
		this.obdTrainMap = obdTrainMap;
		this.filesList = filesList;
		this.obdBedRollMap = obdBedRollMap;
		this.obdLangMap = obdLangMap;
		this.trainDepartureMap = trainDepartureMap;
		this.trainList = trainList;
		this.classBedRoll = classBedRoll;
		this.classBedRollList = classBedRollList;
		this.sdf = sdf;
		this.batchSize = batchSize;
		this.alertPnr = alertPnr;
		this.pnrList = pnrList;
		this.sortedStationCodeList = sortedStationCodeList;
		this.sortedTrainList = sortedTrainList;
		this.createMapTime = createMapTime;
		this.poolConfig = poolConfig;
	}

	public void run()
	{
		try
		{
			try {
				sendDataToApi(filesList, daysBack);
			} catch (ParseException e) {
				System.out.println("System stuckk:::::::"+e.getMessage());
				new Alert().getSendSmsAlert("System stuck . Please restart the process, exception is " + e.getMessage() + " :: for feedback calling data" + ", at time " + new Date());
				System.exit(0);
			}
			return;
		}
		catch (IllegalThreadStateException e)
		{
			System.out.println("System stuckk:::::::" +e.getMessage());
			new Alert().getSendSmsAlert("System stuck . Please restart the process, exception is " + e.getMessage() + " :: for feedback calling data" + ", at time " + new Date());
			System.exit(0);
		}
	}


	private void sendDataToApi(List<File> filesList, int daysBack)
			throws ParseException
	{
		long purgeTime = 0L;
		String unprocessedFolder = "E:\\139_Voice_CDR\\Unprocessed\\Unprocessed" + getCurrentDate() + "\\";
		JedisPool jedisPool = null;
		Calendar cal = Calendar.getInstance();

		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DatabaseManager.getConnection();
			pnrList.clear();
			pnrList = PnrList.listOfPnrNumbers();
			if (pnrList == null) {
				pnrList = new ArrayList<String>();
			}
			System.out.println("pnr list size is " + pnrList.size());



			st = conn.prepareStatement("INSERT INTO tbl_outdial_testing (insert_datetime,pnr_number,msisdn,train_no,station_code,reserved_upto,class,final_status,calling_time,depart_time,obd,obd_cur_status,doj,lang,status,coach,seat_number) values (now(),?,?,?,?,?,?,?,?,?,?,?,?,?,'I',?,?)");



			jedisPool = new JedisPool(poolConfig, "192.168.9.46");
			Jedis jedis = null;
			try {
				jedis = (Jedis)jedisPool.getResource();
			} catch (Exception e) {
				System.err.println("Redis not responding");
				Thread.sleep(3000L);
				jedis = (Jedis)jedisPool.getResource();
			}
			for (File f : filesList)
			{
				purgeTime = System.currentTimeMillis() - daysBack * 8 * 60 * 60 * 1000;
				if (f.lastModified() <= purgeTime)
				{
					try {
						File dirC = new File(unprocessedFolder);
						if (!f.getName().contains(".SMS")) continue;
						if (!dirC.exists()) {
							dirC.mkdir();
						}
						if (f.renameTo(new File(unprocessedFolder + f.getName()))) {
							System.out.println(f + "  File is moved successful!");
						} else {
							System.out.println(f + "  File is failed to move!");
							f.delete();
						}
					}
					catch (Exception e) {
						System.err.println("Error while moving file from source to des");
					}
				}
				else
				{
					totalProcessedCount = 0;
					if ((f.isFile()) && (f.length() > 0L)) {
						String trainNumber = f.getName().substring(0, 5);

						FileReader in = new FileReader(f);
						List<String> lines = IOUtils.readLines(in);

						fileName = f.getName();

						String[] firstLine = ((String)lines.get(0)).split(",");

						String dateStr = firstLine[3].split(":")[0];
						String timeStr = firstLine[3].split(":")[1];
						String departTime = firstLine[1].split(":")[1];

						Date d = sdf.parse(timeStr);

						Calendar c1 = Calendar.getInstance();
						c1.setTime(d);

						c1.add(10, 4);

						int hour = c1.get(10);

						int validLines = 0;
						int invalidLines = 0;
						int dupPnrs = 0;

						int count = 0;
						long insertRecordTime = System.currentTimeMillis();

						for (int i = 1; i < lines.size(); i++) {
							String line = (String)lines.get(i);
							String[] lineArr = line.split(",");
							String pnr = lineArr[2].trim();
							String mobileNumber = lineArr[3].trim();
							String finalStatus = null;
							if ((lineArr.length > 17) && (lineArr[17] != null)) {
								finalStatus = lineArr[17].trim();
							}

							alertPnr = pnr;

							if ((mobileNumber.length() != 10) || (
									(!"CNF".equalsIgnoreCase(finalStatus)) && (!"RAC".equalsIgnoreCase(finalStatus)))) {
								invalidLines++;
							}
							else {
								long checkPnrTime = System.currentTimeMillis();
								if (!pnrList.contains(pnr)) {
									Log.getTimeLogs("time to check in pnr List in if " + (System.currentTimeMillis() - checkPnrTime) + ", of size=" + pnrList.size());
									pnrList.add(pnr);
									String trainNo = lineArr[4].trim();
									String boardingStation = lineArr[7].trim();
									String reservedUpto = lineArr[8].trim();
									String dojDay = lineArr[5].trim();
									String dojMonth = lineArr[6].trim();
									String coachClass = lineArr[11].trim();
									String year = getYear(dojDay + dojMonth);
									String doj = year + "-" + dojMonth + "-" + dojDay;
									String coachNo = lineArr[18].trim();
									String seatNo = lineArr[19].trim();
									TrainOBD obd = new TrainOBD();
									obd.setTrainClass(coachClass);
									obd.setTrainNumber(trainNumber);

									TrainStation ts = new TrainStation();
									ts.setStation(boardingStation);
									ts.setTrain(trainNo);

									String trainDepartTime = (String)trainDepartureMap.get(ts);

									if ((trainDepartTime == null) || ("".equals(trainDepartTime)))
									{
										String h = timeStr.substring(0, 2);
										String m = timeStr.substring(2, 4);
										trainDepartTime = h + ":" + m;
									}

									String dojForCalling = dojDay + dojMonth + year;

									String lang = (String)obdLangMap.get(trainNumber);

									String obdPromt = null;

									if (classBedRollList.contains(obd.getTrainClass())) {
										obdPromt = (String)obdBedRollMap.get(obd);
									}

									if (((obdPromt == null) || ("".equals(obdPromt))) && 
											(sortedStationCodeList != null) && (sortedStationCodeList.contains(boardingStation))) {
										obdPromt = "1";
										cal = Calendar.getInstance();
										int currentHour = cal.get( Calendar.HOUR_OF_DAY );
										if(currentHour >= 15 && currentHour <=20){
											obdPromt = (String)obdTrainMap.get(obd);
											if ((obdPromt == null) || ("".equals(obdPromt))) {
												obdPromt = "1";
											}
										}
										
										
									}

									if ((obdPromt == null) || ("".equals(obdPromt))) {
										obdPromt = (String)obdTrainMap.get(obd);
									}
									
									if ((obdPromt == null) || ("".equals(obdPromt))) {
										continue;
									}
									
									int addHour = 1;

									CallTimePojo pojo = null;


									if ("1".equals(obdPromt)) {
										int addMin = 30;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, addMin);
									} else if ("2".equals(obdPromt))
									{
										addHour = 2;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, 0);
									} else {
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, 0);
									}

									long stationCount = 0L;
									long trainCount = 0L;
									
									if(pojo==null){
										continue;
									}
									
									if(pojo.getCurDate().equals(pojo.getCallingDate())){

									if (obdPromt == "1") {
										stationCount = countFromRedis.readStationCounterRedis(obdPromt, boardingStation, jedis, poolConfig);
									} else {
										trainCount = countFromRedis.readTrainCounterRedis(obdPromt, trainNumber, jedis, poolConfig);
									}
									try
									{
										if (((obdPromt.equals("2")) && (trainCount >= 40L)) || ((obdPromt.equals("0")) && (trainCount >= 60L)) || ((obdPromt.equals("1")) && (stationCount >= 65L)))
										{
											if ((sortedTrainList == null) || (!sortedTrainList.contains(trainNumber))) continue;
											trainCount = countFromRedis.readTrainCounterRedis("0", trainNumber, jedis, poolConfig);
											if (trainCount > 60L) continue;
											if (obdPromt == "1") {
												String obdNew=(String)obdTrainMap.get(obd);
												if(obdNew!=null && obdNew.equals("0"))
												UpdateObdOutdial.getTotalStnCount(obdPromt, boardingStation, trainNumber);
											}
											
											obdPromt = (String)obdTrainMap.get(obd);
											if ((obdPromt == null) || ("".equals(obdPromt))) {
												continue;
											}
										}
									}
									catch (Exception e)
									{

										Log.getErrorLogs(fileName, "No obd obtained:::::::::: coressponding to train ::::: " + trainNumber);
										continue;
									}
								}

									if ((lang == null) || ("".equals(lang))) {
										lang = "h";
									}
									/*int addHour = 1;

									CallTimePojo pojo = null;


									if ("1".equals(obdPromt)) {
										int addMin = 30;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, addMin);
									} else if ("2".equals(obdPromt))
									{
										addHour = 2;
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, 0);
									} else {
										pojo = getCallDateTime(dojForCalling, trainDepartTime, addHour, 0);
									}*/

									if (pojo != null)
									{
										String callingTime = pojo.getCallTime();
										String obdCurStatus = pojo.getObdCurrentStatus();

										ObdRowPojo obdPojo = new ObdRowPojo();
										obdPojo.setBoardingStation(boardingStation);
										obdPojo.setCoachClass(coachClass);
										obdPojo.setCoachNo(coachNo);
										obdPojo.setDepartTime(departTime);
										obdPojo.setDoj(doj);
										obdPojo.setFinalStatus(finalStatus);
										obdPojo.setLang(lang);
										obdPojo.setMobileNumber(mobileNumber);
										obdPojo.setObdCurStatus(obdCurStatus);
										obdPojo.setObdPromt(obdPromt);
										obdPojo.setPnr(pnr);
										obdPojo.setReservedUpto(reservedUpto);
										obdPojo.setSeatNo(seatNo);
										obdPojo.setTrainNo(trainNo);
										obdPojo.setCallingTime(callingTime);

										if (trainList.contains(trainNo)) {
											String status = insertEcatering(obdPojo);
											if ((status != null) && (status.equals("false"))) {
												addRecordInBatch(obdPojo, st);
											}
										} else {
											addRecordInBatch(obdPojo, st);
										}

										count++; if (count % batchSize == 0) {
											st.executeBatch();
											System.out.println("batch uploaded");
											st.clearParameters();
										}

										validLines++;
									}
								} else {
									Log.getTimeLogs(

											"time to check in pnr List in else " + (System.currentTimeMillis() - checkPnrTime) + ", of size=" + pnrList.size());
									dupPnrs++;
								}
							}
						}



						Log.getTimeLogs("Total time to insert and check pnr in list " + validLines + " record of file =" + f.getName() + "--->" + (System.currentTimeMillis() - insertRecordTime) + " Total dulpcate pnr :::" + dupPnrs + " Total valid records:::::::" + validLines);
						System.out.println("Total inserted records :::: " + totalProcessedCount + " from " + lines.size());

						st.executeBatch();
						st.clearParameters();

						Log.getProcessLogs(f.getName() + "," + hour + "," + lines.size() + "," + invalidLines + "," + 
								validLines + "," + dupPnrs);

						System.out.println(f.getName() + "," + hour + "," + lines.size() + "," + invalidLines + "," + 
								validLines + "," + dupPnrs);
						IOUtils.closeQuietly(in);

						long timeToMoveFile = System.currentTimeMillis();
						String doneFilesFolder = "E:\\139_Voice_CDR\\done" + getCurrentDate();

						File doneFolder = new File(doneFilesFolder);
						if ((!doneFolder.isDirectory()) && (!doneFolder.exists())) {
							doneFolder.mkdir();
						}

						String doneFiles = doneFilesFolder + "\\" + f.getName();
						f.delete();

						File destFile = new File(doneFiles);

						FileOutputStream fos = new FileOutputStream(destFile);
						IOUtils.writeLines(lines, "\n", fos);
						IOUtils.closeQuietly(fos);

						Log.getTimeLogs("time to move file --->" + (System.currentTimeMillis() - timeToMoveFile));
					}
				}
			}

			if (jedis.isConnected()) {
				jedis.disconnect();
			}

			while (jedisPool != null) {
				try {
					jedisPool.destroy();
				} catch (Exception e) {
					Thread.sleep(2000);
				}
			}
			Thread.sleep(4000);
		}
		catch (FileNotFoundException e1) {
			Log.getErrorLogs(fileName, "exception in first catch file not found----------> for pnr =" + alertPnr + 
					" exception is= " + e1.getMessage() + ":: for feedback calling data");
			/*new Alert().getSendSmsAlert("In Filename " + fileName + ",first catch for pnr " + alertPnr + 
					", exception is " + e1.getMessage() + " :: for feedback calling data" + ", at time " + new Date());*/
		} catch (SQLException e2) {
			Log.getErrorLogs(fileName, "exception in second catch sql----------> for pnr =" + alertPnr + 
					" exception is= " + e2.getMessage());
			/*new Alert().getSendSmsAlert("In Filename " + fileName + ",second catch for pnr " + alertPnr + 
					", exception is " + e2.getMessage() + " :: for feedback calling data" + ", at time " + new Date());*/
		} catch (Exception e) {
			Log.getErrorLogs(fileName, "exception in third catch normal exception----------> for pnr =" + alertPnr + 
					" exception is= " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(conn);
		}

		System.out.println("final time of complete process " + (System.currentTimeMillis() - createMapTime));

		System.out.println("Thread name " + Thread.currentThread().getName());
	}

	public static String insertEcatering(ObdRowPojo pojo) {
		Connection conn = null;
		CallableStatement st = null;
		String status = "false";
		try
		{
			conn = DatabaseManager.getConnection();
			st = conn.prepareCall("{call proc_outdial_ecatering(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			st.setString(1, pojo.getMobileNumber());
			st.setString(2, pojo.getTrainNo());
			st.setString(3, pojo.getCoachClass());
			st.setString(4, pojo.getBoardingStation());
			st.setString(5, pojo.getCoachNo());
			st.setString(6, pojo.getSeatNo());
			st.setString(7, pojo.getPnr());

			st.setInt(8, obdCounter);

			st.setString(9, "ecatering");
			st.setString(10, pojo.getLang());
			st.setString(11, pojo.getReservedUpto());
			st.setString(12, pojo.getFinalStatus());
			st.setString(13, pojo.getDepartTime());
			st.setString(14, pojo.getDoj());
			st.registerOutParameter(15, 12);
			st.executeUpdate();
			status = st.getString(15);



			if (obdCounter == 5) {
				obdCounter = 0;
				obdCounterFlag = true;
			}

			if (!obdCounterFlag) {
				obdCounter += 1;
			}
			obdCounterFlag = false;
		} catch (Exception e) {
			System.err.println("Error while getting ecatering data:::::::::::::"+e.getMessage());
		} finally {
			DbUtils.closeQuietly(st);
			DbUtils.closeQuietly(conn);
		}
		return status;
	}

	public static void addRecordInBatch(ObdRowPojo pojo, PreparedStatement st) {
		try {
			st.setString(1, pojo.getPnr());
			st.setString(2, pojo.getMobileNumber());
			st.setString(3, pojo.getTrainNo());
			st.setString(4, pojo.getBoardingStation());
			st.setString(5, pojo.getReservedUpto());
			st.setString(6, pojo.getCoachClass());
			st.setString(7, pojo.getFinalStatus());
			st.setString(8, pojo.getCallingTime());
			st.setString(9, pojo.getDepartTime());
			st.setString(10, pojo.getObdPromt());
			st.setString(11, pojo.getObdCurStatus());
			st.setString(12, pojo.getDoj());
			st.setString(13, pojo.getLang());
			st.setString(14, pojo.getCoachNo());
			st.setString(15, pojo.getSeatNo());
			st.addBatch();
			totalProcessedCount += 1;
		} catch (Exception e) {
			System.err.println("Error while inserting data:::::::: "+e.getMessage());
		}
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		return format.format(date);
	}

	public static CallTimePojo getCallDateTimeForObd2(String date, String year, String station, String train, String fileName, String fileTime, String dojDay, String dojMonth) throws Exception
	{
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("ddMMyyyy HH:mm");

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date d = null;

		if ((year != null) && (year.length() == 4)) {
			year = year.substring(2, 4);
		}

		String response = hitUrl(train, year, station, fileName, dojDay, dojMonth);
		String time = getTime(response);
		try
		{
			if ((time != null) && (!"".equals(time))) {
				d = inputDateFormat.parse(String.format("%s %s", new Object[] { date, time }));
			} else {
				d = inputDateFormat.parse(String.format("%s %s", new Object[] { date, fileTime }));
			}
		} catch (Exception e) {
			Log.getErrorLogs(fileName, "error  in date parsing " + e.getMessage());
			System.err.println("Error while parsing wrong date:::::: "+e.getMessage());
		}


		Calendar c = Calendar.getInstance();
		c.setTime(d);

		if ((time == null) || ("".equals(time))) {
			c.add(10, 5);
		} else {
			c.add(10, 3);
		}


		int hour = c.get(11);

		String obdStatus = "";
		if ((hour >= 21) && (hour <= 23)) {
			c.add(5, 1);
			c.set(11, 9);
			c.set(12, 0);
			obdStatus = "backlog";
		} else if (hour < 9) {
			c.set(11, 9);
			c.set(12, 0);
			obdStatus = "backlog";
		} else {
			obdStatus = "current";
		}





		Date finalDate = new Date(c.getTimeInMillis());
		String finalDateTime = formatter.format(finalDate);
		CallTimePojo callTimePojo = new CallTimePojo();
		callTimePojo.setCallTime(finalDateTime);
		callTimePojo.setObdCurrentStatus(obdStatus);
		return callTimePojo;
	}

	public static CallTimePojo getCallDateTime(String date, String time, int addHour, int addMin) {
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("ddMMyyyy HH:mm");

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date curDate = new Date();
		String curDateTime = formatter.format(curDate);


		Date d = null;
		CallTimePojo callTimePojo = new CallTimePojo();

		try
		{
			try{
				d = inputDateFormat.parse(String.format("%s %s", new Object[] { date, time }));
			}catch (Exception e) {
				Log.getErrorLogs("Error", "While parsing date");
				return null;
			}

			Calendar c = Calendar.getInstance();
			c.setTime(d);

			if (addMin > 0) {
				c.add(12, addMin);
			} else {
				c.add(10, addHour);
			}

			int hour = c.get(11);

			String obdStatus = "";
			if ((hour >= 21) && (hour <= 23)) {
				c.add(5, 1);
				c.set(11, 9);
				c.set(12, 0);
				obdStatus = "backlog";
			}
			else if (hour < 9) {
				c.set(11, 9);
				c.set(12, 0);
				obdStatus = "backlog";
			}
			else {
				obdStatus = "current";
			}
			Date finalDate = new Date(c.getTimeInMillis());
			String finalDateTime = formatter.format(finalDate);
			callTimePojo = new CallTimePojo();

			Date currentDate = formatter.parse(curDateTime);
			if (currentDate.compareTo(finalDate) > 0) {
				return null;
			}
			callTimePojo.setCallTime(finalDateTime);
			callTimePojo.setObdCurrentStatus(obdStatus);
			callTimePojo.setCallingDate(dateFormat.parse(dateFormat.format(finalDate)));
			callTimePojo.setCurDate(dateFormat.parse(dateFormat.format(currentDate)));
		} catch (Exception e) {
			System.out.println("Error while generating calling datetime ::::::::: "+e.getMessage());
		}
		return callTimePojo;
	}

	public static String getYear(String strDDMM) {
		String str_inmonth = strDDMM.substring(2);
		int in_month = Integer.parseInt(str_inmonth);
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(2) + 1;
		int year = calendar.get(1);
		if (in_month < month) {
			year++;
		}
		String strYear = Integer.toString(year);
		String str = strYear;
		return str;
	}

	public static String hitUrl(String trainNUmber, String year, String stationCode, String fileName, String dojDay, String dojMonth)
			throws Exception
	{
		String ip = Utilities.getConfKeyValue("ip");

		String doj = dojDay + dojMonth + year;

		String url = "http://" + ip + "/ArrivalDeparture/trainrequest.aspx?trnno=%s&doj=%s&stdcode=%s";
		url = String.format(url, new Object[] { trainNUmber, doj, stationCode });
		Log.getProcessLogs("url is:- " + url);
		InputStream in = null;
		InputStreamReader reader = null;
		BufferedReader br = null;
		String resp = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection)obj.openConnection();

			con.setRequestMethod("GET");
			con.setConnectTimeout(3000);
			con.setReadTimeout(3000);






			in = con.getInputStream();
			reader = new InputStreamReader(in);
			br = new BufferedReader(reader);

			StringBuffer response = new StringBuffer();
			String inputLine;
			while ((inputLine = br.readLine()) != null) { 
				response.append(inputLine);
			}
			resp = response.toString();
			System.out.println("response of url is " + resp);
			Log.getProcessLogs("reponse of url:- " + url + " is -" + resp);
		} catch (Exception e) {
			Log.getErrorLogs(fileName, "error is " + e.getMessage());
			
		} finally {
			if (in != null) {
				in.close();
			}
			if (reader != null) {
				reader.close();
			}
			if (br != null) {
				br.close();
			}
		}
		return resp;
	}

	static String getTime(String apiResponse)
	{
		String resp = "";

		if ((apiResponse != null) && (apiResponse.contains("#"))) {
			String[] response = apiResponse.split("#");
			if ((response != null) && (response.length >= 4)) {
				String expectedArrival = response[4];
				if ((expectedArrival != null) && (expectedArrival.contains(":"))) {
					expectedArrival = expectedArrival.substring(expectedArrival.indexOf(":"));
					if (expectedArrival.contains(",")) {
						String[] times = expectedArrival.split(",");
						if ((times != null) && (times.length > 1)) {
							resp = times[1].trim();
						}
					}
				}
			}
		}
		return resp;
	}
}
