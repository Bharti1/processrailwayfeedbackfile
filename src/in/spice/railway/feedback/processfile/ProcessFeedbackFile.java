package in.spice.railway.feedback.processfile;

import in.spice.railway.feedback.EcateringTrains;
import in.spice.railway.feedback.OBDLangMap;
import in.spice.railway.feedback.TrainDeparture;
import in.spice.railway.feedback.TrainList;
import in.spice.railway.feedback.TrainOBD;
import in.spice.railway.feedback.TrainOBDMap;
import in.spice.railway.feedback.TrainStation;
import in.spice.railway.feedback.config.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import redis.clients.jedis.JedisPoolConfig;

public class ProcessFeedbackFile
{
  static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
  static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  static Calendar cal = Calendar.getInstance();
  
  static Date sTime = null;
  static Date eTime1 = null;
  

  static Time startTime = null;
  static Time endTime = null;
  static Time eTime = null;
  
  static String startDate = null;
  static String endDate = null;
  static String endCallingDate = null;
  static String startCallingDate = null;
  
  static long difference = 0L;
  static int count = 0;
  

  static Properties prop = new Properties();
  
  public ProcessFeedbackFile() {}
  
  public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
    try { 
      
      Process proc = Runtime.getRuntime().exec("WMIC path win32_process get Caption,Processid,Commandline");
      BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
      OutputStreamWriter oStream = new OutputStreamWriter(proc.getOutputStream());
      
      oStream.flush();
      oStream.close();
      int i = 0;
      String processLine; 
      while ((processLine = input.readLine()) != null) { 
        if (processLine.contains("ProcessRailwayFile.jar")) {
          i++;
          if (i > 1) {
            processLine = processLine.replaceAll("\\D", "");
            processLine = processLine.substring(4, processLine.length());
            System.out.println("processLine id is " + processLine);
            String currentProcessId = ManagementFactory.getRuntimeMXBean().getName();
            System.out.println("original currentProcessId " + currentProcessId);
            currentProcessId = currentProcessId.split("@")[0];
            System.out.println("currentProcessId is " + currentProcessId);
            if (processLine.equals(currentProcessId)) {
              System.out.println("multiple processes");
              System.exit(0);
            }
          }
          System.out.println("first process id of manag " + ManagementFactory.getRuntimeMXBean().getName());
          System.out.println("Single process.");
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    

    long elapsedTime = 0L;
    boolean isThreadAlive = false;
    try
    {
      for (;;) {
        timerCode();
        Thread.sleep(600000L);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  


  private static void timerCode()
    throws FileNotFoundException, IOException, ParseException, InterruptedException
  {
    long createMapTime = System.currentTimeMillis();
    Map<TrainOBD, String> obdTrainMap = TrainOBDMap.insertIntoMap("0");
    Map<TrainOBD, String> obdBedRollMap = TrainOBDMap.insertIntoMap("2");
    Map<String, String> obdLangMap = OBDLangMap.insertIntoMap();
    Map<TrainStation, String> trainDepartureMap = TrainDeparture.insertIntoMap();
    List<String> trainList = EcateringTrains.listOfTrains();
    String[] classBedRoll = { "2A", "3A", "3E", "1A" };
    List<String> classBedRollList = Arrays.asList(classBedRoll);
    
    Log.getTimeLogs("time to create maps " + (System.currentTimeMillis() - createMapTime));
    
    long createPnrList = System.currentTimeMillis();
    List<String> pnrList = new ArrayList<String>();
    int batchSize = 700;
    
    String alertPnr = "";
    
    String dirName = "E:\\139_Voice_CDR\\";
    

    SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
    
    File dir = new File(dirName);
    
    File[] dirListing = dir.listFiles();
    
    List<String> sortedTrainList = new ArrayList<String>();
    List<String> sortedStationCodeList = new ArrayList<String>();
    sortedTrainList = TrainList.selectedTrainList();
    sortedStationCodeList = TrainList.selectedStationCode();
    List<File> filesList = Arrays.asList(dirListing);
    
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxIdle(120);
    poolConfig.setMinIdle(2);
    poolConfig.setTestOnBorrow(true);
    
    new RequestProcessor(obdTrainMap, filesList, obdBedRollMap, obdLangMap, 
      trainDepartureMap, trainList, classBedRoll, classBedRollList, sdf, batchSize, 
      alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
  }
}