package in.spice.railway.feedback.processfile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import in.spice.feedback.constants.Constants;
import in.spice.railway.feedback.TrainOBD;
import in.spice.railway.feedback.TrainStation;
import redis.clients.jedis.JedisPoolConfig;


public class RequestProcessor
{
  private List<File> fileList = new ArrayList<File>();
  private List<File> fileList1 = new ArrayList<File>();
  private List<File> fileList2 = new ArrayList<File>();
  private List<File> fileList3 = new ArrayList<File>();
  private List<File> fileList4 = new ArrayList<File>();
  private List<File> fileList5 = new ArrayList<File>();
  private List<File> fileList6 = new ArrayList<File>();
  
  private Map<TrainOBD, String> obdTrainMap = new HashMap<TrainOBD, String>();
  private List<File> filesList = new ArrayList<File>();
  private Map<TrainOBD, String> obdBedRollMap = new HashMap<TrainOBD, String>();
  private Map<String, String> obdLangMap = new HashMap<String, String>();
  private Map<TrainStation, String> trainDepartureMap = new HashMap<TrainStation, String>();
  private List<String> trainList = new ArrayList<String>();
  private String[] classBedRoll = null;
  private List<String> classBedRollList = new ArrayList<String>();
  private static int totalProcessedCount = 0;
  private SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
  private int batchSize = 0;
  private String alertPnr;
  private List<String> pnrList = new ArrayList<String>();
  private List<String> sortedStationCodeList = new ArrayList<String>();
  private List<String> sortedTrainList = new ArrayList<String>();
  private long createMapTime = 0L;
  
  private JedisPoolConfig poolConfig;
  private int i = 0;
  private ExecutorService executorService = null;
  private int loopCount = 0;
  



  public RequestProcessor(Map<TrainOBD, String> obdTrainMap, List<File> filesList, Map<TrainOBD, String> obdBedRollMap, Map<String, String> obdLangMap, Map<TrainStation, String> trainDepartureMap, List<String> trainList, String[] classBedRoll, List<String> classBedRollList, SimpleDateFormat sdf, int batchSize, String alertPnr, List<String> pnrList, List<String> sortedStationCodeList, List<String> sortedTrainList, long createMapTime, JedisPoolConfig poolConfig)
    throws InterruptedException
  {
    this.obdTrainMap = obdTrainMap;
    this.filesList = filesList;
    this.obdBedRollMap = obdBedRollMap;
    this.obdLangMap = obdLangMap;
    this.trainDepartureMap = trainDepartureMap;
    this.trainList = trainList;
    this.classBedRoll = classBedRoll;
    this.classBedRollList = classBedRollList;
    this.sdf = sdf;
    this.batchSize = batchSize;
    this.alertPnr = alertPnr;
    this.pnrList = pnrList;
    this.sortedStationCodeList = sortedStationCodeList;
    this.sortedTrainList = sortedTrainList;
    this.createMapTime = createMapTime;
    this.poolConfig = poolConfig;
    process();
  }
  
  private void process() throws InterruptedException {
    executorService = 
      Executors.newFixedThreadPool(Constants.TOTAL_THREAD_IN_POOL);
    int totalCount = filesList.size();
    int countProcess = totalCount /Constants.TOTAL_THREAD_IN_POOL;
    int rangTo = 0;
    int remainder = 0;
    int rangeFrom = 0;
    try
    {
      if (totalCount >= Constants.TOTAL_THREAD_IN_POOL) {
        for (i = 0; i < Constants.TOTAL_THREAD_IN_POOL; i += 1) {
          if (totalCount % countProcess == 0)
          {
            rangTo = rangeFrom;
            if (rangeFrom == 0) {
              rangeFrom = countProcess;
            }
            rangeFrom = rangTo + countProcess;
            isThreadProcessed(rangTo, rangeFrom, i);
          }
          else {
            remainder = totalCount % countProcess;
            totalCount -= totalCount % countProcess;
            rangeFrom = totalCount /Constants.TOTAL_THREAD_IN_POOL;
            isThreadProcessed(rangTo, remainder, i);
          }
        }
      } else {
        if (loopCount == 0) {
          fileList1.addAll(fileList);
          Runnable processor = new MiddleWare(fileList1, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
        if (loopCount == 1) {
          fileList2.addAll(fileList);
          Runnable processor = new MiddleWare(fileList2, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
        if (loopCount == 2) {
          fileList3.addAll(fileList);
          Runnable processor = new MiddleWare(fileList3, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
        if (loopCount == 3) {
          fileList4.addAll(fileList);
          Runnable processor = new MiddleWare(fileList4, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
        if (loopCount == 4) {
          fileList5.addAll(fileList);
          Runnable processor = new MiddleWare(fileList5, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
        if (loopCount == 5) {
          fileList6.addAll(fileList);
          Runnable processor = new MiddleWare(fileList6, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
            classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
          executorService.execute(processor);
        }
      }
      



      if (remainder > 0) {
        remainder = rangeFrom + remainder;
        isThreadProcessed(rangeFrom, remainder, i);
        System.out.println(rangeFrom + " to " + remainder + "::::::::::::::::: thread " + i);
      }
      





      executorService.shutdown();
      
      while (executorService.isTerminated()) {}
    }
    catch (Exception e)
    {
      System.out.println("System Exit :::::::: memory issue");
      System.exit(0);
    }
  }
  
  public void isThreadProcessed(int rangTo, int rangeFrom, int i) throws InterruptedException
  {
    List<File> railWifiList7 = new CopyOnWriteArrayList<File>();
    List<File> railWifiList8 = new CopyOnWriteArrayList<File>();
    List<File> railWifiList9 = new CopyOnWriteArrayList<File>();
    List<File> railWifiList10 = new CopyOnWriteArrayList<File>();
    List<File> railWifiList11 = new CopyOnWriteArrayList<File>();
    List<File> railWifiList12 = new CopyOnWriteArrayList<File>();
    
    int lastIndex = filesList.size() - 1;
    if (lastIndex < rangTo) {
      return;
    }
    if (filesList.size() == 1) {
      Runnable processor = new MiddleWare(new CopyOnWriteArrayList<File>(fileList), obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
    
    if (i == 0)
    {
      System.out.println("railWifiList7 " + rangTo + "," + rangeFrom);
      if ((lastIndex < rangeFrom) && (fileList.size() > 0)) rangeFrom = lastIndex;
      railWifiList7 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList7, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
    else if (i == 1)
    {
      System.out.println("railWifiList8 " + rangTo + "," + rangeFrom);
      if (lastIndex < rangeFrom) rangeFrom = lastIndex;
      railWifiList8 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList8, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
    else if (i == 2)
    {
      System.out.println("railWifiList9 " + rangTo + "," + rangeFrom);
      if (lastIndex < rangeFrom) rangeFrom = lastIndex;
      railWifiList9 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList9, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
    else if (i == 3)
    {
      System.out.println("railWifiList10 " + rangTo + "," + rangeFrom);
      if (lastIndex < rangeFrom) rangeFrom = lastIndex;
      railWifiList10 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList10, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
    else if (i == 4)
    {
      System.out.println("railWifiList11 " + rangTo + "," + rangeFrom);
      if (lastIndex < rangeFrom) rangeFrom = lastIndex;
      railWifiList11 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList11, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);

    }
    else if (i == 5)
    {
      System.out.println("railWifiList12 " + rangTo + "," + rangeFrom);
      if (lastIndex < rangeFrom) rangeFrom = lastIndex;
      railWifiList12 = new CopyOnWriteArrayList<File>(filesList.subList(rangTo, rangeFrom));
      Runnable processor = new MiddleWare(railWifiList12, obdTrainMap, obdBedRollMap, obdLangMap, trainDepartureMap, trainList, classBedRoll, 
        classBedRollList, sdf, batchSize, alertPnr, pnrList, sortedStationCodeList, sortedTrainList, createMapTime, poolConfig);
      executorService.execute(processor);
    }
  }
}