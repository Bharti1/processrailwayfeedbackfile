package in.spice.feedback.constants;

public abstract interface Constants
{
  public static final int TOTAL_THREAD_IN_POOL = 3;
}